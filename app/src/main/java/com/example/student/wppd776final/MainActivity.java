package com.example.student.wppd776final;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ClipData;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.student.wppd776final.fragments.BlankFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        createFrag();
        //THIS works on normal AVD emulator but not Genymotion PLEASE use physical keyboard
        InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        View v = (View) findViewById(R.id.main);
        keyboard.showSoftInput(v,0);
        MenuItem item = (MenuItem) findViewById(R.id.about);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.about) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void createFrag()
    {

        FragmentManager manager = getFragmentManager();
        FragmentTransaction fragmentTransaction = manager.beginTransaction();
        BlankFragment fragment = new BlankFragment();
        fragmentTransaction.replace(R.id.main,fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

    }

    public void clickFahrenheit(View v)
    {
        TextView text = (TextView) findViewById(R.id.textViewOne);
        TextView display = (TextView) findViewById(R.id.textView);
        EditText input = (EditText) findViewById(R.id.editText);
        if(input.getText().equals("") || input.getText().equals(null))
        {

            inputError();
        }
        else
        {
            try
            {
                String valueStr = input.getText().toString();
                Double value = Double.parseDouble(valueStr);
                value = (9.0/5.0*value) + 32.0;
                Character symbol = 0x00B0;
                display.setText(valueStr + symbol + " converted to Fahrenheit is: " + value + symbol + "F.");

            }
            catch(Exception e)
            {
                e.printStackTrace();
                inputError();
            }
        }
    }

    public void clickCelsius(View v)
    {
        TextView text = (TextView) findViewById(R.id.textViewOne);
        TextView display = (TextView) findViewById(R.id.textView);
        EditText input = (EditText) findViewById(R.id.editText);
        if(input.getText().equals("") || input.getText().equals(null))
        {

            inputError();
        }
        else
        {
            try
            {
                String valueStr = input.getText().toString();
                Double value = Double.parseDouble(valueStr);
                value = (5.0/9.0)*(value-32);
                Character symbol = 0x00B0;
                display.setText(valueStr + symbol + " converted to Celsius is: " + value + symbol + "C.");

            }
            catch(Exception e)
            {
                e.printStackTrace();
                inputError();
            }
        }
    }

    public void inputError()
    {
        Toast.makeText(this,"Please enter a number!",Toast.LENGTH_LONG).show();
        EditText input = (EditText) findViewById(R.id.editText);
        input.setText("");

    }


    public void menuAbout(MenuItem item)
    {

        setContentView(R.layout.about);
        FragmentManager manager = getFragmentManager();
        FragmentTransaction fragmentTransaction = manager.beginTransaction();
        BlankFragment fragment = new BlankFragment();
        fragmentTransaction.replace(R.id.about,fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

    }
}
